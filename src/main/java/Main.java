import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import database.DbService;
import servlets.SigninServlet;
import servlets.SignupServlet;
import user_mgmt.UserService;

/**
 * Created by Registered on 19.12.2016.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        DbService dbService = new DbService();
        UserService userService = new UserService(dbService);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(new SignupServlet(userService)), "/signup");
        context.addServlet(new ServletHolder(new SigninServlet(userService)), "/signin");

        Server server = new Server(8080);
        server.setHandler(context);

        server.start();
        System.out.println("Server started");
        server.join();
    }
}
