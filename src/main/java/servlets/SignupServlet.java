package servlets;

import user_mgmt.UserProfile;
import user_mgmt.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Registered on 19.12.2016.
 */
public class SignupServlet extends HttpServlet {
    private UserService userService;

    public SignupServlet(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        if (login == null || password == null) {
            resp.setContentType("text/html;charset=utf-8");
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        resp.setContentType("text/html;charset=utf-8");
        if (login != null && userService.GetUser(login) == null) {
            userService.InsertUser(new UserProfile(login, password));
        }
        resp.setStatus(HttpServletResponse.SC_OK);
    }
}
