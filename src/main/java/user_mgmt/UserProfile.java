package user_mgmt;

/**
 * Created by Registered on 19.12.2016.
 */
public class UserProfile {
    private Long id;
    private String login;
    private String password;

    public UserProfile(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
