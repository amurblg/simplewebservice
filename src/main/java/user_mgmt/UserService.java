package user_mgmt;

import database.DbService;
import database.UserDataSet;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 * Created by Registered on 19.12.2016.
 */
public class UserService {
    private DbService dbService;

    public UserService(DbService dbService) {
        this.dbService = dbService;
    }

    public Long InsertUser(UserProfile userProfile) throws HibernateException {
        org.hibernate.Session session = dbService.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Long result = (Long) session.save(new UserDataSet(userProfile.getLogin(), userProfile.getPassword()));
        transaction.commit();
        return result;
    }

    public UserProfile GetUser(String userName) throws HibernateException {
        Criteria criteria = dbService.getSessionFactory().openSession().createCriteria(UserDataSet.class);
        UserDataSet uds = (UserDataSet) criteria.add(Restrictions.eq("login", userName)).uniqueResult();
        if (uds == null) return null;

        UserProfile result = new UserProfile(uds.getLogin(), uds.getPassword());
        return result;
    }

    public UserProfile GetUser(Long userId) throws HibernateException {
        UserDataSet uds = dbService.getSessionFactory().openSession().get(UserDataSet.class, userId);
        if (uds == null) return null;

        UserProfile result = new UserProfile(uds.getLogin(), uds.getPassword());
        return result;
    }
}
